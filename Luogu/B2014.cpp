//Luogu - B2014
#include<bits/stdc++.h>

using namespace std;
constexpr double PI = 3.14159;
double r;

int main() {
    cin >> r;
    printf("%.4f %.4f %.4f", 2 * r, 2 * PI * r, PI * r * r);
    return 0;
}
